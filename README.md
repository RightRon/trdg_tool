# 针对trdg包开发的工具——可以随机生成预设属性的图像
本项目是对（https://github.com/Belval/TextRecognitionDataGenerator.gitxianmu)项目的工具包。
本工具包可以更加简单的设置想要生成图片的属性。
*（Ubuntu环境）*
## 环境配置
*	Python3.7+
*	trdg
*	tqdm
## 库的安装
*	方法一
  1. 选择python环境
  2. pip install trdg
*	方法二
  1. 预先创建的文件夹
  2. 在文件夹中右键选择（打开终端）
  3. 在命令行中输入（git clone https://github.com/Belval/TextRecognitionDataGenerator.git)
  4. 包就成功下载到当前文件夹中

## 此项目下载
1. 打开本trgd安装的文件
2. 右键打开终端
3. 输入（git clone https://gitee.com/RightRon/trdg_tool.git)
## 项目文件介绍
1. `trdg_tool.py` :本项目核心代码，使用时run此文件。
2. `cn.txt`:本项目准备的语料。
3. `colormap.txt`:预选颜色文件。
## TRDG_TOOL类参数介绍
1.  `blur_range`：图片模糊参数
  * 随机模糊，取值可以输入一个范围`[low,high]`,模糊值会随机在范围内取。
* 也可以输入一个`数值`，这样取值就是从（0～`数值`）中取一个。
  ***（输入必须是整数）***
2. `txt_path`：输入要生成文本`.txt`文件的路径。
3. `text_length_range`：文本长度参数，随机文本的长度，必须输入>1的整数。
4. `colors`：随机颜色参数
* 输入列表类型，列表元素是colormap.txt中的字符串，可输入多个元素，输出是从中选择的元素。（**字符串名字就是颜色**）。
* 列表元素也可以按照颜色顺序`R，G，B`，将这三个颜色通道的数值转化为16进制，在把这三个16进制数按照字符串格式进行拼接，可输入多个元素。
* `skewing_angle`：随机角度参数，输入类型是整数，角度范围在（输入，到0—输入）之间。
5. `count`：生成图片的个数，输入为整数。
6. `font`, `language`：字体参数
* 如果language=Null,则font要传入一个列表，列表元素是字体的路径，输出是随机选择的字体。
* 如果`language=""cn"`,则字体直接读入`trdg/fonts/cn`文件夹下的字体。
* 如果`language=""其他"`，则字体直接读入`trdg/fonts/latin`文件夹下的字体。
7. `size`:字号大小，输入一个整数。字号不会随机，输入多少就是多少。

## 运行参数配置
**打开`trdg_tool.py`文件**
1. 在最下面修改 **图片保存路径** 和 **标签保存路径**
2. 按照之前参数的介绍，在类`TRDG_TOOL`实例化为`generator`时根据自己要生成图像的要求选择合适参数。



